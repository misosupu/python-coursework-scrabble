__author__ = 'vazov'

_end = '_end_'


class Trie:
    def __init__(self, *words):
        self.root = dict()
        for word in words:
            current_dict = self.root
            for letter in word:
                current_dict = current_dict.setdefault(letter, {})
            current_dict.setdefault(_end, _end)

    def add_word(self, word):
        subtrie = self.root.setdefault(word[0], {})
        if len(word) == 1:
            subtrie.setdefault(_end, _end)
        else:
            add_word(subtrie, word[1:])

    def in_trie(self, word):
        current_dict = self.root
        for letter in word:
            if letter in current_dict:
                current_dict = current_dict[letter]
            else:
                return False
        else:
            if _end in current_dict:
                return True
            else:
                return False

    def print_trie(self):
        print(self.root)


def load_words(filename):
    words = []
    with open(filename, 'r') as file:
        for line in file:
            words.append(line.strip())
    return words

'''
dictionary = load_words("/home/vazov/Downloads/enable1.txt")
t = Trie(*dictionary)
# t.print_trie()
print(t.in_trie("creep"))
'''