import random
import wordtrie
import re


class Cell:
    def __init__(self):
        self.type = "+"
        self.is_empty = True
        self.holds_letter = ''
        self.multiply_by = 1

    def place_tile(self, tile):
        self.is_empty = False
        self.holds_letter = tile

    def __str__(self):
        if self.is_empty is True:
            return self.type
        else:
            return self.holds_letter

    def __len__(self):
        if self.is_empty is True:
            return len(self.type)
        else:
            return len(self.holds_letter)


class TripleWordCell(Cell):
    def __init__(self):
        super().__init__()
        self.type = "TW"


class DoubleWordCell(Cell):
    def __init__(self):
        super().__init__()
        self.type = "DW"


class TripleLetterCell(Cell):
    def __init__(self):
        super().__init__()
        self.type = "TL"
        self.multiply_by = 3


class DoubleLetterCell(Cell):
    def __init__(self):
        super().__init__()
        self.type = "DL"
        self.multiply_by = 2


class Board:
    __triple_word_indexes = [(i, j) for i in range(0, 15, 7) for j in range(0, 15, 7)]
    __double_word_indexes = [(1, 1), (2, 2), (3, 3), (4, 4),
                             (1, 13), (2, 12), (3, 11), (4, 10),
                             (13, 1), (12, 2), (11, 3), (10, 4),
                             (14, 13), (13, 12), (12, 11), (11, 10)]
    __triple_letter_indexes = [(5, 1), (9, 1), (2, 5), (6, 5), (10, 5), (13, 5),
                               (2, 10), (6, 10), (10, 10), (13, 10),
                               (13, 1), (13, 1)]
    __double_letter_indexes = [(0, 3), (0, 11), (2, 6), (2, 8),
                               (3, 0), (3, 7), (3, 14),
                               (6, 2), (6, 6), (6, 8), (6, 12),
                               (7, 3), (7, 11),
                               (8, 2), (8, 6), (8, 8), (8, 12),
                               (11, 0), (11, 7), (11, 14),
                               (14, 3), (14, 11), (14, 6), (14, 8)]

    def __init__(self):
        self.grid = [[Cell() for _ in range(15)] for _ in range(15)]  # every element is an ordinary cell
        # place special cells:
        for index in Board.__triple_word_indexes:
            self.grid[index[0]][index[1]] = TripleWordCell()
        for index in Board.__double_word_indexes:
            self.grid[index[0]][index[1]] = DoubleWordCell()
        for index in Board.__triple_letter_indexes:
            self.grid[index[0]][index[1]] = TripleLetterCell()
        for index in Board.__double_letter_indexes:
            self.grid[index[0]][index[1]] = DoubleLetterCell()

    def show_board(self):
        """
        TODO: probably useless
        """
        def stringify(elem):
            if len(elem) >= 3:
                raise UnboundLocalError
            return str(elem) + ' ' * (3 - len(elem))

        def print_row(n, row):
            n_indent = "" if n >= 9 else " "
            print(str(n + 1) + n_indent, "  ".join(stringify(elem) for elem in row))

        print('   ', end='')  # TODO
        print("    ".join("ABCDEFGHIJKLMNO"))
        for number, row in enumerate(self.grid):
            print_row(number, row)

    def get_cell(self, row, column):
        return self.grid[row][column]

    def place_tiles(self, start, end, tiles):
        # start and end must be formatted as follows:
        # start = '[A-O]:[1-15]'
        # start = [letter, index], end = [letter, index]

        if start[0] is end[0]:  # eg. vertically
            # cringe-inducing:
            k = 0
            for i in range(int(start[1]) - 1, int(end[1])):
                self.grid[i][ord(start[0]) - 65].place_tile(tiles[k])
                k += 1
        else:  # eg. horizontally
            k = 0
            for i in range(ord(start[0]) - 65, ord(end[0]) - 64):
                self.grid[int(start[1]) - 1][i].place_tile(tiles[k])
                k += 1

    def remove_tiles(self, coordinates):
        # remove invalid word - eg error with word intersection validation
        for (i, j) in coordinates:
            self.grid[i][j] = Cell()
            self.words_by_index[(i, j)] = ['', '']

    def range_is_free(self, coordinates):
        taken = set()
        for (i, j) in coordinates:
            if self.get_cell(i, j).is_empty is False:
                taken.add((i, j))
        if len(taken) > 0:
            return taken
        else:
            return None

    def get_horizontal_word(self, row, column, letter):
        # letter variable hold the (hypothetical) letter at (row, column)
        word = letter
        # go right
        i = column + 1
        while not self.grid[row][i].is_empty and i <= 14:
            word += self.grid[row][i].holds_letter
            i += 1
        # go left
        i = column - 1
        while not self.grid[row][i].is_empty and i >= 0:
            word = self.grid[row][i].holds_letter + word
            i -= 1

        if len(word) > 1:
            # return word
            return word, (row, i)  # where (i, j) is the index of the first letter of the word
        else:
            return None

    def get_vertical_word(self, row, column, letter):
        # letter variable holds the (hypothetical) letter at (row, column)
        word = letter
        word = self.grid[row][column].holds_letter
        # go down
        i = row + 1
        while not self.grid[row][i].is_empty and i <= 14:
            word += self.grid[row][i].holds_letter
            i += 1
        # go up
        i = row - 1
        while not self.grid[i][column].is_empty and i >= 0:
            word = self.grid[i][column].holds_letter + word
            i -= 1

        if len(word) > 1:
            # return word
            return word, (i, column)  # where (i, j) is the index of the first letter of the word
        else:
            return None


class Bag:
    def __init__(self):
        self.bag = {
            'a': [1, 9], 'b': [3, 2], 'c': [3, 2],
            'd': [2, 4], 'e': [1, 12], 'f': [4, 2],
            'g': [2, 3], 'h': [4, 2], 'i': [1, 9],
            'j': [8, 1], 'k': [5, 1], 'l': [1, 4],
            'm': [3, 2], 'n': [1, 6], 'o': [1, 8],
            'p': [3, 2], 'q': [10, 1], 'r': [1, 6],
            's': [1, 4], 't': [1, 6], 'u': [1, 4],
            'v': [4, 2], 'w': [4, 2], 'x': [8, 1],
            'y': [4, 2], 'z': [10, 1]
        }

    def draw_tiles(self, number_of_tiles):
        available_tiles = [y for key in self.bag.keys() for y in [key] * self.bag[key][1] if self.bag[key][1] != 0]
        l = random.sample(available_tiles, number_of_tiles)
        for elem in l:
            self.bag[elem][1] -= 1
        return [(letter, self.bag[letter][0]) for letter in l]
    
    def return_tiles(self, to_replace):
        # TODO: check if to_replace is a subset of players tiles
        for elem in to_replace:
            value = self.bag.get(elem)
            value[1] += 1
            self.bag[elem] = [value[0], value[1]]

    def is_empty(self):
        """check that all values for the letters are equal to 0"""
        return len({key for key in self.bag if self.bag[key][1] != 0}) == 0
    
    def current_contents(self):
        for elem in self.bag:
            return self.bag[elem]


class TileHolder:
    def __init__(self, bag):
        self.tiles = bag.draw_tiles(7)
    
    def __str__(self):
        return "The holder has the following letters {}".format(self.tiles)

    def add(self, letters):
        self.tiles.extend(letters)

    def remove_letters(self, letters_with_points, bagy):
        # TODO: function trying to remove letters from previous turn in current word
        for letter in letters_with_points:
            self.tiles.remove(letter)
            [score, quantity] = bagy.bag[letter[0]]
            quantity += 1
            bagy.bag[letter[0]] = [score, quantity]

    def empty(self):
        self.tiles.clear()

    def draw_letters(self, n, bagy):
        self.tiles.extend(bagy.draw_tiles(n))


class Player(TileHolder):
    """ Attributes:
            name = player name 
            letters = list of the 7 letters the player currently has 
            points = players score
    """

    def __init__(self, name, bag):
        self.name = name
        self.points = 0
        TileHolder.__init__(self, bag)

    def __str__(self):
        return "Player %s has %s points so far." % (self.name, self.points) 

    def get_turn_info(self, word, score):
        print("Player {} played the word \"{}\" for {} points.".format(self.name, word, score))

    def update_points(self, new_score):
        self.points += new_score

    def withdraw(self):
        pass


class Validator:
    def __init__(self):
        def load_words(filename):
            words = []
            with open(filename, 'r') as file:
                for line in file:
                    words.append(line.strip())
                return words
        dictionary = load_words("words.txt")
        self.trie = wordtrie.Trie(*dictionary)

    def validate_word(self, word):
        if self.trie.in_trie(word):
            return True
        else:
            raise InputNotAWordError(word)

        # return True

    @staticmethod
    def __validate_letters(player, word):
        """
        Checks if the letters in word are contained by player's rack
        :param player:
        :param word:
        :return: True or NoSuchTileError(letter)
        """

        rack_copy = player.tiles
        for letter in word:
            # TODO: this is going to be matched: word - aaron #player's rack - aron
            if letter not in [elem[0] for elem in rack_copy]:
                raise NoSuchTileError(letter)
            else:
                # TODO: remove works only on string and we want to remove a tuple >.>
                for (l, _) in rack_copy:
                    if l == letter:
                        rack_copy.remove((l, _))
                        break
        return True

    def validate_intersections(self, intersections):
        for word in intersections:
            try:
                self.validate_word(word)
            except InvalidWordError:
                return False
        return True

    @staticmethod
    def validate_index(coordinates):
        if not re.match(r'^[A-O]:[1-9](?!\w)|^[A-O]:1[0-5]$', coordinates):
            return False
        return True


class History:
    def __init__(self):
        """
        holds list of words containing tuples of the sort: (action_type, word, points, turn, player_name)
        """
        self.words = []

    def document_turn(self, turn, player_name, action, word, score):
        self.words.append((turn, player_name, action, word, score))

    def is_duplicate(self, word):
        for record in self.words:
            if word == record[3]:
                return record
        return False


class Controller:
    def __init__(self):
        self.players = []  # holds list of active players
        self.round = 1

    def add_player(self, player_name, bag):
        self.players.append(Player(player_name, bag))

    def remove_player(self, player_name):
        for player in self.players:
            if player.name == player_name:
                self.players.remove(player)

    def determine_first_player(self):
        return random.randint(1, len(self.players))

    @staticmethod
    def __get_intersecting_words(layout, full_coordinates, partial_coordinates, word, board):
        """
        :param layout: either 'horizontal' or 'vertical'
        :param partial_coordinates: coordinates range on board for word
        :param word:
        :param board:
        :return: intersecting_words is in the form [(word, (i_start, j_start)]
        """
        intersecting_words = []

        for (i, j) in partial_coordinates:
            # if we write a word vertically - check horizontal word crosses
            # else - check vertical word crosses
            index = full_coordinates.index((i, j))  # holds index of letter in word, corresponding to (i, j) on board

            if layout == 'vertical':  # eg - vertical
                result = board.get_horizontal_word(i, j, word[index])
                if result is not None:  # => result = (word, start_cell_indexes)
                    horizontal_word = result[0]
                    start_cell = result[1]
                    end_cell = (start_cell[0], start_cell[1] + len(horizontal_word) - 1)
                    intersecting_words.append((horizontal_word, start_cell, end_cell))
            else:  # eg - layout == 'horizontal'
                result = board.get_vertical_word(i, j, word[index])
                if result is not None:
                    vertical_word = result[0]
                    start_cell = result[1]
                    end_cell = (start_cell[0] + len(vertical_word) - 1, start_cell[1])
                    intersecting_words.append((vertical_word, start_cell, end_cell))
        return intersecting_words

    @staticmethod
    def get_word_input():
        word = input("Word: ")
        #  TODO: dont break on error
        if not word.isalpha():
            raise InputNotAWordError(word)
        else:
            return word

    @staticmethod
    def get_index(validator, place):
        index = input("Enter {} tile: ".format(place))
        if not validator.validate_index(index):
            raise IncorrectIndexError()
        else:
            return index

    @staticmethod
    def __choose_action(game_info):
        game_info.list_actions()
        action = input("Enter action number: ")
        while action not in '1234':
            action = input("Enter action number: ")
        return action

    @staticmethod
    def calculate_points(word, s, e):
                # TODO: calculate intersection words as well
                # "word" is in the form [(letter, points),...]
                triple_word_flag = False
                double_word_flag = False
                points = 0
                if s[0] == e[0]:  # eg - vertically
                    i = int(s[1]) - 1
                    for (letter, score) in word:
                        points += score*board.grid[ord(s[0]) - 65][i].multiply_by
                        if board.grid[ord(s[0]) - 65][i].type is "TW" and board.grid[ord(s[0]) - 65][i].is_empty:
                            triple_word_flag = True
                        elif board.grid[ord(s[0]) - 65][i].type is "DW" and board.grid[ord(s[0]) - 65][i].is_empty:
                            double_word_flag = True
                        i += 1

                else:  # eg - horizontally
                    i = ord(s[0]) - 65
                    for (letter, score) in word:
                        points += score*board.grid[i][int(s[1]) - 1].multiply_by
                        if board.grid[i][int(s[1]) - 1].type is "TW" and board.grid[i][int(s[1]) - 1].is_empty:
                            triple_word_flag = True
                        elif board.grid[i][int(s[1]) - 1].type is "DW":
                            double_word_flag = True
                        i += 1

                if triple_word_flag is True:
                    return points * 3
                elif double_word_flag is True:
                    return points * 2
                else:
                    return points

    @staticmethod
    def get_coordinates_input(validator):
            try:
                start = Controller.get_index(validator, "start")
            except IncorrectIndexError:
                raise IncorrectStartIndexInputError()
            try:
                end = Controller.get_index(validator, "end")
            except IncorrectIndexError:
                raise IncorrectEndIndexInputError()

            return start, end

    @staticmethod
    def get_coordinates_range(start, end):
        coordinates_range = []
        if start[0] == end[0]:  # eg - vertically
            for j in range(int(start[1]) - 1, int(end[1])):
                coordinates_range.append((j, (ord(start[0]) - 65)))
        else:  # eg - horizontally
            for i in range(ord(start[0]) - 65, ord(end[0]) - 64):
                coordinates_range.append((int(start[1]) - 1, i))
        return coordinates_range

    def play_word(self, validator, bagy, player, board, game_history, game_info):
        # 1. play word
        word = Controller.get_word_input()
        word_with_points = [(letter, bagy.bag[letter][0]) for letter in word]
        start, end = Controller.get_coordinates_input(validator)

        start = start.split(':')
        end = end.split(':')

        # get list of letters which are part of the board
        coordinates = Controller.get_coordinates_range(start, end)
        full_coordinates = coordinates  # keeps list of the full coordinates list (needed for intersections check)

        # check if middle cell is in the range if it's the first turn
        if self.round == 1:
            while (7, 7) not in coordinates:
                game_info.invalid_initial_word()
                start, end = Controller.get_coordinates_input(validator)
                coordinates = Controller.get_coordinates_range(start, end)

        # TODO: don't allow a move to be made if it doesn't intersect with another word - refer to above function
        filled_cells = board.range_is_free(coordinates)
        if filled_cells is None and self.round != 1:
            # TODO: make into separate function
            flag = True
            for (i, j) in coordinates:
                if start[0] == end[0]:  # eg - horizontal
                    if board.get_cell(i, j + 1).is_empty and board.get_cell(i, j - 1).is_empty:
                        flag = False
                        break
                else:  # eg - vertical
                    if board.get_cell(i + 1, j).is_empty and board.get_cell(i - 1, j).is_empty:
                        flag = False
                        break
            if flag is False:
                # eg - every cell in the range is free => there is no intersection with an existing word - invalid
                raise TheWordDoesNotIntersectError(coordinates)

        existing_tiles = []  # contains the tiles, which are already on the board, prior to writing player's word

        # check if every letter in word has a corresponding tile on board
        player_rack = [element[0] for element in player.tiles]  # contains the letters only != (letter, points)
        for (index, coordinate) in enumerate(coordinates):
            if not board.get_cell(coordinate[0], coordinate[1]).holds_letter == word[index]:
                if word[index] not in player_rack:
                    raise NoSuchTileInRackError(word[index], player.name)
            else:
                existing_tiles.append(word_with_points[index])

        # equivalent to the set difference function, applied for lists (preserve multiple equal elements)
        player_tiles = [element for element in word_with_points if element not in existing_tiles]
        if filled_cells is not None:  # eg - there are tiles in the word, already on the board
            coordinates = set(coordinates).difference(filled_cells)

        # check if word has already been played:
        result = game_history.is_duplicate(word)
        if result is True:  # TypeError: 'bool' object is not iterable
            raise DuplicateWordError(result[1], result[3])
        if validator.validate_word(word):
            if start[0] == end[0]:  # eg - vertical
                intersections = Controller.__get_intersecting_words('vertical', full_coordinates,
                                                                    coordinates, word, board)
            else:
                intersections = Controller.__get_intersecting_words('horizontal', full_coordinates,
                                                                    coordinates, word, board)

            if validator.validate_intersections(intersections) is True:
                # calculate points for intersections, since they are legitimate words:
                word_points = 0
                for item in intersections:
                    word_points += Controller.calculate_points(item[0], item[1][0], item[1][1])

                word_points += Controller.calculate_points(word_with_points, start, end)
                player.update_points(word_points)
                player.get_turn_info(word, word_points)
                # update board:
                board.place_tiles(start, end, word)
                player.remove_letters(player_tiles, bagy)
                player.add(bagy.draw_tiles(len(player_tiles)))
                self.round += 1  # update turn number
                # document turn in history - document_turn(self, turn, player_name, action, word, score)
                game_history.document_turn(self.round, player.name, "play", word, word_points)
                game_info.turn_info(player.name, word, word_points)
        else:
            # raise InvalidWordError(word)
            game_info.invalid_word_played(word, player.name)
            # print("The word {} player \"{}\" has entered is incorrect. Invalid move.".format(word, player.name))

        # print("Write \"{}\" from {} to {}".format(word, start, end))

    @staticmethod
    def change_letters(bagy, game_info):
        # change letters
        # TODO: check if letters are part of player's rack
        letters = input("Choose letters to remove: ")
        letters = [(letter, bagy.bag[letter][0]) for letter in list(letters)]
        player.remove_letters(letters, bagy)
        player.add(bagy.draw_tiles(len(letters)))
        game_info.player_passive(player.name, "change tiles")
        # print("Player \"{}\" changed tiles.".format(player.name))

    def turn(self, validator, bagy, player, board, game_history, game_info):
        action = Controller.__choose_action(game_info)
        if action == '1':
            self.play_word(validator, bagy, player, board, game_history, game_info)
        elif action == '2':
            game_info.player_passive(player.name, "changed letters")
            Controller.change_letters(bagy, game_info)
        elif action == '3':
            # pass
            game_info.player_passive(player.name, "passed their turn")
        else:
            # resign
            self.resign_game(player)

    def __determine_winner(self):
        players_with_scores = []
        for player in self.players:
            players_with_scores.append((player.name, player.points))
        players_with_scores = sorted(players_with_scores, key=lambda x: x[1])
        print("The winner is player \"{}\" with points {}".format(players_with_scores[-1][0], players_with_scores[-1][1]))
        self.players.clear()

    def end_game(self):
        return self.__determine_winner()

    def resign_game(self, player):
        if len(self.players) - 1 > 1:
            print("Player \"{}\" has resigned.".format(player.name))
            self.remove_player(player)
        else:
            return self.__determine_winner()


class GameInfo:
    @staticmethod
    def show_board(game_board):
        def stringify(elem):
            if len(elem) >= 3:
                raise UnboundLocalError
            return str(elem) + ' ' * (3 - len(elem))

        def print_row(n, row):
            n_indent = "" if n >= 9 else " "
            print(str(n + 1) + n_indent, "  ".join(stringify(elem) for elem in row))
        print('   ', end='')  # TODO
        print("    ".join("ABCDEFGHIJKLMNO"))
        for number, row in enumerate(game_board.grid):
            print_row(number, row)

    @staticmethod
    def rack_info(player):
        print(player.tiles)

    @staticmethod
    def turn_info(player_name, word, score):
        print("Player {} played the word \"{}\" for {} points.".format(player_name, word, score))

    @staticmethod
    def list_actions():
        print("What would you like to do?")
        print("1. play word\n2. change letters\n3. pass\n4. resign")

    @staticmethod
    def invalid_initial_word():  # line 511
        print("First word cells should intersect with cell (8, 8).\n"
              "Enter valid coordinates for start and end.")

    @staticmethod
    def turn_info(player_name, word, word_points):  # line 553
        print("Player \"{}\" played the word \"{}\" for {} points.".format(player_name, word, word_points))

    @staticmethod
    def player_passive(player_name, action):  # line 586
        print("Player \"{}\" has {}.".format(player_name, action))

    @staticmethod
    def player_changed_tiles(player_name):  # line 586
        print("Player \"{}\" has made a move - change tiles.".format(player_name))

    @staticmethod
    def announce_winner(winner):
        print("The winner is player \"{}\" with {} points".format(winner.name, winner.score))

    @staticmethod
    def announce_turn(player):
        print("Player \"{}\"'s turn.".format(player.name))

    @staticmethod
    def invalid_word_played(word, player_name):
        print("The word {} player \"{}\" has entered is incorrect. Invalid move.".format(word, player_name))

class InputNotAWordError(Exception):
    def __init__(self, word):
        self.message = "Input \"{}\" not a word.".format(word)

    def __str__(self):
        return repr(self.message)


class InputNotANumberError(Exception):
    def __init__(self):
        self.message = "Input not a number."

    def __str__(self):
        return repr(self.message)


class InvalidWordError(Exception):
    def __init__(self, word):
        self.message = "Word \"{}\" does not exist.".format(word)

    def __str__(self):
        return repr(self.message)


class IncorrectIndexError(Exception):
    def __init__(self):
        self.message = "Incorrect cell index."

    def __str__(self):
        return repr(self.message)


class IncorrectStartIndexInputError(IncorrectIndexError):
    def __init__(self):
        self.message = "Incorrect start cell index."


class IncorrectEndIndexInputError(IncorrectIndexError):
    def __init__(self):
        self.message = "Incorrect end cell index."


class DuplicateWordError(Exception):
    def __init__(self, player_name, word):
        self.message = "Word \"{0}\" has already been played by player \"{1}\""\
            .format(word, player_name)

    def __str__(self):
        return repr(self.message)


class CellsAlreadyFilledError(Exception):
    def __init__(self, start_cell, end_cell):
        self.message = "The selected range {} to {} has more than 1 filled cells".format(start_cell, end_cell)


class NoSuchTileError(Exception):
    def __init__(self, tile):
        self.message = "Tile \"{}\" is not part of player's rack. Invalid word".format(tile)


class IntersectionWordError(Exception):
    def __init__(self, cell):
        self.message = "The intersection at cell {} does not form a valid word".format(cell)


class NoSuchTileAtCellError(Exception):
    def __init__(self, tile, cell):
        self.message = "Tile \"{}\" does not exist on cell {}. Invalid word".format(tile, cell)


class NoSuchTileInRackError(Exception):
    def __init__(self, tile, player_name):
        self.message = "Tile \"{}\" does not exist on player \"{}\"'s rack. Invalid word".format(tile, player_name)


class TheWordDoesNotIntersectError(Exception):
    def __init__(self, coordinates):
        self.message = "The selected cell range {} does not intersect with any played word. Invalid move."\
            .format(coordinates)


board = Board()
board.show_board()

info = GameInfo()
valid = Validator()
bag = Bag()
history = History()

c = Controller()

c.add_player("Player 1", bag)
c.add_player("Player 2", bag)

while True:
    if len(c.players) == 0:
        break
    for player in c.players:
        info.announce_turn(player)
        info.rack_info(player)
        c.turn(valid, bag, player, board, history, info)
        info.show_board(board)
